Catalog module
==============
Catalog module

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist vilderr/catalog "*"
```

or add

```
"vilderr/catalog": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \vilderr\catalog\AutoloadExample::widget(); ?>```