<?php

namespace vilderr\catalog;

use vilderr\main\Yii;
use yii\base\BootstrapInterface;

class Module extends \vilderr\main\base\Module implements BootstrapInterface
{
    public function bootstrap($app)
    {
        $app->i18n->translations["modules/catalog/*"] = [
            'class' => 'yii\i18n\PhpMessageSource',
            'forceTranslation' => true,
            'basePath' => '@vendor/vilderr/catalog/messages',
            'fileMap' => [
                'modules/catalog/module' => 'module.php'
            ],
        ];

        if ($app->id == 'backend') {
            $app->urlManager->addRules([
                [
                    'class' => 'yii\\web\\GroupUrlRule',
                    'prefix' => 'catalog',
                    'routePrefix' => 'catalog',
                    'rules' => [
                        '' => 'catalog/index'
                    ]
                ],
            ]);
        }
    }

    public static function t($message, $category = 'module', $params = [], $language = null)
    {
        return Yii::t('modules/catalog/' . $category, $message, $params, $language);
    }

    public function getName()
    {
        return self::t('Catalog module');
    }

    public function getMenuItems()
    {
        $items = [
            'settings' => [
                'items' => [
                    'modules' => [
                        'items' => [
                            300 => [
                                'label' => static::getName(),
                                'url' => ['/main/settings/default/index', 'module_id' => 'catalog'],
                            ]
                        ]
                    ]
                ]
            ]
        ];

        return $items;
    }
}